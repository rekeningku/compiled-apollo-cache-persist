import Log from './Log';
import Storage from './Storage';
import Cache from './Cache';
import { ApolloPersistOptions } from './types';
export interface PersistorConfig<T> {
    log: Log<T>;
    cache: Cache<T>;
    storage: Storage<T>;
}
export default class Persistor<T> {
    log: Log<T>;
    cache: Cache<T>;
    storage: Storage<T>;
    maxSize?: number;
    paused: boolean;
    whitelist?: Array<string>;
    blacklist?: Array<string>;
    persistMutation?: boolean;
    constructor({ log, cache, storage }: PersistorConfig<T>, options: ApolloPersistOptions<T>);
    filterMap(map: {
        [key: string]: any;
    }, filterFn: (key: string) => boolean): {
        [key: string]: any;
    };
    searchList(list: Array<string>, key: string, isPersistMutation?: boolean): boolean;
    persist(): Promise<void>;
    restore(): Promise<void>;
    purge(): Promise<void>;
}
